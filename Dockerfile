ARG UBUNTU_RELEASE=bionic
ARG UBUNTU_RELEASE_VERSION=${UBUNTU_RELEASE}-20200112

FROM ubuntu:${UBUNTU_RELEASE_VERSION}

LABEL maintainer="Matt Copperwaite matt@copperwaite.net"

ARG UBUNTU_RELEASE
ARG UBUNTU_RELEASE_VERSION
ARG VIRTUALBOX_VERSION=6.1
ARG VIRTUALBOX_INSTALL_VERSION=6.1.2-135662~Ubuntu~bionic

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && apt-get install -y --no-install-recommends curl=7.58.0-2ubuntu3.8 ca-certificates=20180409 gnupg2=2.2.4-1ubuntu1.2 && \
    echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian ${UBUNTU_RELEASE} contrib" > /etc/apt/sources.list.d/virtualbox.list && \
    curl -qL https://www.virtualbox.org/download/oracle_vbox_2016.asc | apt-key add - && \
    curl -qL https://www.virtualbox.org/download/oracle_vbox.asc | apt-key add - && \
    apt-get update && apt-get install -y --no-install-recommends virtualbox-${VIRTUALBOX_VERSION}=${VIRTUALBOX_INSTALL_VERSION} && \
    rm /etc/apt/sources.list.d/virtualbox.list && apt-get clean && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["vboxmanage"]
