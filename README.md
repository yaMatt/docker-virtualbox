# Docker VirtualBox
You might think I'm mad putting VirtualBox in to docker. And sure, I would have no idea how or why you would run a VM inside Docker. However VirtualBox supplies a tool called [VBoxManage](https://www.virtualbox.org/manual/ch08.html) that lets you manipulate and control offline virtual images. Including converting them from one type of virtual machine to another. Now that can be pretty handy!

## Usage

```

```

## Features
- Fully linted
- Fully tested
- Regular releases to update base image
- Lowest privileges
- Fixed versions
- Easy to contribute and maintain to because:
  - Use of [args](https://docs.docker.com/engine/reference/builder/#arg)
  - CI/CD
  - Friendly community

## Contribute

Hi, if you would like to contribute, and I would really appreciate it if you did, you can find and clone the repo on my [personal GitLab](https://git.copperwaite.net/matt/docker-virtualbox).

## License

### AGPLv3

    Copyright (C) 2019  Matt Copperwaite

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
